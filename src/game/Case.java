package game;

import java.util.Hashtable;

public class Case {
	private boolean haut;
	private boolean bas;
	private boolean gauche;
	private boolean droite;
	
	/* --- Constructeur --- */
	public Case(boolean h, boolean b, boolean g, boolean d) {
		Constructeur(h,b,g,d);
	}
	public Case(){
		this(true, true, true, true);
	}
	public Case(Hashtable<Character, Boolean> c) throws Exception {
		try {
			Constructeur(c.get('h'), c.get('b'), c.get('g'), c.get('d'));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void Constructeur(boolean h, boolean b, boolean g, boolean d) {
		haut = h;
		bas = b;
		gauche = g;
		droite = d;
	}
	
	/* --- Getter --- */
	public boolean isHaut() {
		return haut;
	}
	public boolean isBas() {
		return bas;
	}
	public boolean isGauche() {
		return gauche;
	}
	public boolean isDroite() {
		return droite;
	}
	
	/* --- Polymorphisme --- */
	@Override
	public String toString() {
		return "\n\tCase [haut=" + haut + ", bas=" + bas + ", gauche=" + gauche
				+ ", droite=" + droite + "]";
	}
	
	
	
}
