package game;

import java.util.Random;

public class Alea {
	/* --- systeme de bornes --- */
	/**
	 * % de chance de reussite de création de porte
	 */
	public static final byte OpenDoor = 60;
	public static Random r = new Random();
	
	public static boolean genererOD() {
		return (r.nextInt(100) < OpenDoor);
	}
	
	public static int random(int min, int max){
		return min == max ? min : min + r.nextInt(max+1 - min);
	}
}
