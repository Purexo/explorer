package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * map est une liste bidimentionelle dynamique
 * La liste racine est a visualiser à la verticale et fournis la coordonné Y 
 * 	(Axe principale - => +)
 * Les liste contenu dans la liste racines sont a visualiser a l'horyzontale, et fournie la coordonné x
 * 	(Axe secondaire - => +)
 */

public class GameMap {
	private HashMap<Long, HashMap<Long, Case>> map;
	private long x; // position horizontale sur la map
	private long y; // position verticale sur la map

	private final long X = 0;
	private final long Y = 0;
	
	public GameMap() {
		this.x = X;
		this.y = Y;
		
		/* --- Instantiation de la map et de sa case 0,0 --- */
		map = new HashMap<Long, HashMap<Long, Case>>();
		map.put(y, new HashMap<Long, Case>());
		map.get(y).put(x, new Case());
	}
	
	/* --- Changement de Case --- */
	/**
	 * Si la case sur la quelle on souhaite aller elle est accessible
	 * 	On s'y deplace
	 * 	Si elle n'existe pas
	 * 		On la genere
	 * 	On l'affiche
	 * Sinon
	 * 	avertissement // "Chemin bloqué"
	 * 
	 */
	public void haut() {
		if (map.get(this.y).get(this.x).isHaut()) {
			long newy = y-1;
			if (!testExistanceY(newy)){
				map.put(newy, new HashMap<Long, Case>());
			}
			
			try {
				map.get(newy).put(x, new Case(argsCase(x, newy)));
			} catch (Exception e) { e.printStackTrace(); }
			
			y = newy;
		}
		else {
			System.out.println("Impossible d'aller en haut !");
		}
	}
	public void bas() {
		if (map.get(this.y).get(this.x).isBas()) {
			long newy = y+1;
			if (!testExistanceY(newy)){
				map.put(newy, new HashMap<Long, Case>());
			}
			
			try {
				map.get(newy).put(x, new Case(argsCase(x, newy)));
			} catch (Exception e) { e.printStackTrace(); }
			y = newy;
		}
		else {
			System.out.println("Impossible d'aller en bas !");
		}
	}
	public void gauche() {
		if (map.get(y).get(x).isGauche()) {
			long newx = x-1;
			if (!testExistanceX(newx)){
				try {
					map.get(y).put(newx, new Case(argsCase(newx, y)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			x = newx;
		}
		else {
			System.out.println("Impossible d'aller à gauche !");
		}
	}
	public void droite() {
		if (map.get(this.y).get(this.x).isDroite()) {
			long newx = x+1;
			if (!testExistanceX(newx)){
				try {
					map.get(y).put(newx, new Case(argsCase(newx,y)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			x = newx;
		}
		else {
			System.out.println("Impossible d'aller à droite !");
		}
	}
	
	/* --- Test d'existance d'une case --- */
	/**
	 * permet de determiner si il y a besoin de generer une nouvelle case à l'emplacement donné
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean testExistanceXY(Long x, Long y){
		return map.containsKey(y) ? map.get(y).containsKey(x) : false;
	}
	public boolean testExistanceY(Long y) {
		return map.containsKey(y);
	}
	public boolean testExistanceX(Long x) {
		return map.get(this.y).containsKey(x);
	}
	
	/* --- Case --- */
	/**
	 * test les cases autour de la case courante
	 * afin de renvoyer un tableau associatif des ouvertures à générer
	 * @return
	 */
	public Hashtable<Character, Boolean> argsCase(Long x, Long y){
		Hashtable<Character, Boolean>
		cas = new Hashtable<Character, Boolean>();
		
		cas.put('h', testExistanceXY(x, y-1) ? map.get(y-1).get(x).isBas() : Alea.genererOD());
		cas.put('b', testExistanceXY(x, y+1) ? map.get(y+1).get(x).isHaut() : Alea.genererOD());
		cas.put('g', testExistanceXY(x-1, y) ? map.get(y).get(x-1).isDroite() : Alea.genererOD());
		cas.put('d', testExistanceXY(x+1, y) ? map.get(y).get(x+1).isGauche() : Alea.genererOD());
		
		return cas;
	}
	
	/* --- Test --- */
	public void run() {
		for(byte i = 0; i < 10; i++) {
			ArrayList<Character> cas = deplacement();
			switch (cas.get(Alea.random(0, cas.size()-1))) {
				case 'h': haut(); System.out.print("haut - "); break;
				case 'b': bas(); System.out.print("bas - "); break;
				case 'g': gauche(); System.out.print("gauche - "); break;
				case 'd': droite(); System.out.print("droite - "); break;
			}
		}
	}

	/* --- Possibilite de deplacement --- */
	public ArrayList<Character> deplacement() {
		ArrayList<Character> cas = new ArrayList<Character>();
		
		if (map.get(y).get(x).isHaut())		cas.add('h');
		if (map.get(y).get(x).isBas()) 		cas.add('b');
		if (map.get(y).get(x).isGauche()) 	cas.add('g');
		if (map.get(y).get(x).isDroite()) 	cas.add('d');
		
		return cas;
	}
	
	/* --- Polymorphisme --- */
	@Override
	public String toString() {
		return "GameMap [x=" + x + ", y=" + y + ", map=" + map + "]";
	}
	
	/* ---  --- */
}
